﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gestion_Utilisateur
{
    public class Client
    {
        string login;
        string password;
        int argent;

        public string Login { get => login; set => login = value; }
        public string Password { get => password; set => password = value; }
        public int Argent { get => argent; set => argent = value; }
    }
}