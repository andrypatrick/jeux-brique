package com.example.admin123.myapplication;

/**
 * Created by Admin123 on 03/04/2018.
 */

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback{
    private GameThread thread;
    private Balle balle;
    private Plateau plateau;
    private Brique brique;
    private Brique[] listeBrique;
    private int point;

    public GameView(Context c){
        super(c);
        getHolder().addCallback(this);
        thread = new GameThread(this);
        balle = new Balle(this.getContext());
        plateau = new Plateau(this.getContext());
        //brique = new Brique(this.getContext(),25,25);
        listeBrique = new Brique[18];
        int ind=0,x=45,y=150,p=1;
        while(ind<listeBrique.length){
            if(ind%6==0 && ind !=0){
                x=45;
                y+=90;
                p++;
            }
            listeBrique[ind] = new Brique(this.getContext(),x,y,p);
            System.out.println(listeBrique[ind].getPoint());
            x+=170;
            ind++;
        }
    }

    public void doDraw(Canvas c){
        if(c==null){
            return;
        }
        Paint p = new Paint();
        p.setColor(Color.WHITE);
        p.setTextSize(70);
        c.drawBitmap(BitmapFactory.decodeResource(getResources(),R.mipmap.fond), 0, 0, null);
        c.drawText("Score: "+balle.getPoint(),50,80,p);
        balle.draw(c);
        plateau.draw(c);
        //brique.draw(c);
        for(int i=0;i<listeBrique.length;i++){
            listeBrique[i].draw(c);
        }
    }

    public void update(){
        balle.deplacerCollision(plateau,listeBrique);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if(thread.getState()==Thread.State.TERMINATED) {
            thread=new GameThread(this);
        }
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        balle.redimensioner(width, height);
        plateau.redimensioner();
        //brique.redimensioner();
        for(int i=0;i<listeBrique.length;i++){
            listeBrique[i].redimensioner();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        thread.setRunning(false);
        while(retry){
            try{
                thread.join();
                retry=false;
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int currentX = (int)event.getX();

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                if(currentX >= plateau.getX() && currentX <= plateau.getX()+plateau.getLargeur() ) {
                    plateau.deplacer( false);
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if(!plateau.mouvement()) {
                    plateau.setX(currentX);
                }
                break;

            case MotionEvent.ACTION_UP:
                plateau.deplacer(true);
                balle.deplacer(true);
        }

        return true;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
}
