package com.example.admin123.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Jeu extends AppCompatActivity {
    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameView = new GameView(this);
        setContentView(gameView);
    }
}
